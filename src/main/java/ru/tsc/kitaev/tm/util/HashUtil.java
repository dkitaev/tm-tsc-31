package ru.tsc.kitaev.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kitaev.tm.api.service.IPropertyService;

public interface HashUtil {

    @NotNull
    static String salt(@NotNull final IPropertyService setting, @NotNull final String value) {
        @NotNull final String secret = setting.getPasswordSecret();
        @NotNull final Integer iteration = setting.getPasswordIteration();
        return salt(secret, iteration, value);
    }

    @NotNull
    static String salt(
            @NotNull final String secret,
            @NotNull final Integer iteration,
            @NotNull final String value
    ) {
        @NotNull String result = value;
        for (int i = 0; i < iteration; i++) {
            result = md5(secret + result + secret);
        }
        return result;
    }

    @NotNull
    static String md5(@NotNull final String value) {
        try {
            @NotNull java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            final byte[] array = md.digest(value.getBytes());
            @NotNull final StringBuilder sb = new StringBuilder();
            for (final byte b : array) {
                sb.append(Integer.toHexString((b & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (@NotNull final java.security.NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

}
