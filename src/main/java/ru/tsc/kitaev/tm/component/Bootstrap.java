package ru.tsc.kitaev.tm.component;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.tsc.kitaev.tm.api.repository.ICommandRepository;
import ru.tsc.kitaev.tm.api.repository.IProjectRepository;
import ru.tsc.kitaev.tm.api.repository.ITaskRepository;
import ru.tsc.kitaev.tm.api.repository.IUserRepository;
import ru.tsc.kitaev.tm.api.service.*;
import ru.tsc.kitaev.tm.command.AbstractCommand;
import ru.tsc.kitaev.tm.constant.TerminalConst;
import ru.tsc.kitaev.tm.enumerated.Role;
import ru.tsc.kitaev.tm.exception.system.UnknownCommandException;
import ru.tsc.kitaev.tm.repository.CommandRepository;
import ru.tsc.kitaev.tm.repository.ProjectRepository;
import ru.tsc.kitaev.tm.repository.TaskRepository;
import ru.tsc.kitaev.tm.repository.UserRepository;
import ru.tsc.kitaev.tm.service.*;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.util.SystemUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

@Getter
@Setter
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    private final Backup backup = new Backup(this);

    public void start(@Nullable final String[] args) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        initRegistry();
        initData();
        runArgs(args);
        initPID();
        logService.debug("Test environment");
        @NotNull final Scanner scanner = new Scanner(System.in);
        @NotNull String command = "";
        backup.init();
        while (!TerminalConst.EXIT.equals(command)) {
            try {
                System.out.println("ENTER COMMAND:");
                command = scanner.nextLine();
                logService.command(command);
                runCommand(command);
                logService.info("Completed");
            } catch (@NotNull final Exception e) {
                logService.error(e);
            }
        }
        backup.stop();
    }

    @SneakyThrows
    private void initRegistry() {
        @NotNull final Reflections reflections = new Reflections("ru.tsc.kitaev.tm.command");
        @NotNull final List<Class<? extends AbstractCommand>> classes = reflections
                .getSubTypesOf(ru.tsc.kitaev.tm.command.AbstractCommand.class)
                .stream()
                .sorted(Comparator.comparing(Class::getName))
                .collect(Collectors.toList());

        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            if (Modifier.isAbstract(clazz.getModifiers())) continue;
            registry(clazz.newInstance());
        }
    }

    private void initData() {
        String userId = userService.create("test", "test", "test@email.ru").getId();
        String adminId = userService.create("admin", "admin", Role.ADMIN).getId();
        projectService.create(userId, "Project C", "-").setStatus(Status.COMPLETED);
        projectService.create(userId, "Project A", "-");
        projectService.create(adminId, "Project B", "-").setStatus(Status.IN_PROGRESS);
        projectService.create(adminId, "Project D", "-").setStatus(Status.NOT_STARTED);
        taskService.create(userId, "Task C", "-").setStatus(Status.COMPLETED);
        taskService.create(userId, "Task A", "-");
        taskService.create(adminId, "Task B", "-").setStatus(Status.IN_PROGRESS);
        taskService.create(adminId, "Task D", "-").setStatus(Status.NOT_STARTED);
    }

    public void runArgs(@Nullable final String[] args) {
        if (args == null || args.length == 0) return;
        @Nullable final AbstractCommand command = commandService.getCommandByArg(args[0]);
        if (command == null) throw new UnknownCommandException();
        command.execute();
    }

    public void runCommand(@Nullable final String command) {
        if (command == null || command.isEmpty()) return;
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new UnknownCommandException();
        @Nullable final Role[] roles = abstractCommand.roles();
        authService.checkRoles(roles);
        abstractCommand.execute();
    }

    private void registry(@Nullable final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

}
